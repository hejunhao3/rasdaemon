Name:			rasdaemon
Version:		0.8.0
Release:		7
License:		GPLv2
Summary:		Utility to get Platform Reliability, Availability and Serviceability (RAS) reports via the Kernel tracing events
URL:			https://github.com/mchehab/rasdaemon.git
Source0:		https://github.com/mchehab/rasdaemon/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

ExcludeArch:		s390 s390x
BuildRequires:  	gcc, gettext-devel, perl-generators, sqlite-devel, systemd, git, libtool, libtraceevent-devel
Provides:		bundled(kernel-event-lib)
Requires:		hwdata
Requires:		perl-DBD-SQLite
Requires:		libtraceevent
%ifarch %{ix86} x86_64
Requires:		dmidecode
%endif

Requires(post):		systemd
Requires(preun):	systemd
Requires(postun):	systemd

Patch0: backport-Fix-potential-overflow-with-some-arrays-at-page-isol.patch
Patch1: fix-ras-mc-ctl.service-startup-failed-when-selinux-is-no.patch

Patch9000: bugfix-rasdaemon-wait-for-file-access.patch
Patch9001: bugfix-fix-fd-check.patch
Patch9002: bugfix-fix-disk-error-log-storm.patch
Patch9003: 0001-rasdaemon-Fix-for-regression-in-ras_mc_create_table-.patch
Patch9004: 0001-rasdaemon-fix-return-value-type-issue-of-read-write-.patch
Patch9005: 0002-rasdaemon-fix-issue-of-signed-and-unsigned-integer-c.patch
Patch9006: 0003-rasdaemon-Add-support-for-creating-the-vendor-error-.patch
Patch9007: backport-Check-CPUs-online-not-configured.patch
Patch9008: backport-rasdaemon-diskerror-fix-incomplete-diskerror-log.patch
Patch9009: 0001-rasdaemon-Fix-for-vendor-errors-are-not-recorded-in-.patch
Patch9010: bugfix-fix-cpu-isolate-errors-when-some-cpus-are-.patch
Patch9011: backport-ras-events-quit-loop-in-read_ras_event-when-kbuf-data-is-broken.patch
Patch9012: backport-Rasdaemon-Fix-autoreconf-build-error.patch
Patch9013: backport-mce-amd-smca-update-smca_hwid-to-use-smca_bank_types.patch

%description
The  rasdaemon  program  is  a  daemon which monitors the platform
Reliablity, Availability and Serviceability (RAS) reports from the
Linux kernel trace events. These trace events are logged in
/sys/kernel/debug/tracing, reporting them via syslog/journald.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoscan
aclocal
autoconf
autoheader
libtoolize --automake --copy --debug --force
automake --add-missing
%ifarch %{arm} aarch64
%configure --enable-mce --enable-aer --enable-sqlite3 --enable-extlog --enable-abrt-report --enable-devlink --enable-diskerror --enable-non-standard --enable-hisi-ns-decode --enable-arm --enable-memory-failure --enable-memory-ce-pfa --enable-cpu-fault-isolation
%else
%configure --enable-mce --enable-aer --enable-sqlite3 --enable-extlog --enable-abrt-report --enable-devlink --enable-diskerror
%endif
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
install -D -p -m 0644 misc/rasdaemon.env %{buildroot}%{_sysconfdir}/sysconfig/%{name}
install -D -p -m 0644 misc/rasdaemon.service %{buildroot}/%{_unitdir}/rasdaemon.service
install -D -p -m 0644 misc/ras-mc-ctl.service %{buildroot}%{_unitdir}/ras-mc-ctl.service
rm INSTALL %{buildroot}/usr/include/*.h

%files
%doc ChangeLog README.md TODO
%license AUTHORS COPYING
%{_sbindir}/rasdaemon
%{_sbindir}/ras-mc-ctl
%{_mandir}/*/*
%{_unitdir}/*.service
%{_sysconfdir}/ras/dimm_labels.d
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}

%post
/usr/bin/systemctl enable rasdaemon.service >/dev/null 2>&1 || :
if [ $1 -eq 2 ] ; then
    /usr/bin/systemctl try-restart rasdaemon.service >/dev/null 2>&1 || :
fi

%preun
/usr/bin/systemctl disable rasdaemon.service >/dev/null 2>&1 || :

%changelog
* Tue Jul 16 2024 wangziliang <wangziliang@kylinos.cn> - 0.8.0-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: backport patches from upstream

* Thu Jul 11 2024 zhangxingrong-<zhangxingrong@uniontech.cn> - 0.8.0-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:ras-events: quit loop in read_ras_event when kbuf data is broken and Rasdaemon: Fix autoreconf build error

* Thu Apr 25 2024 yangjunshuo <yangjunshuo@huawei.com> - 0.8.0-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix cpu isolate errors when some cpus are offline
  before the service started

* Fri Mar 29 2024 Bing Xia <xiabing12@h-partners.com> - 0.8.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix for vendor errors are not recorded in the SQLite database if some cpus
       are offline at the system start.

* Wed Mar 27 2024 zhuofeng <zhuofeng2@huawei.com> - 0.8.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix ras-mc-ctl.service startup failed when selinux is on

* Mon Mar 25 2024 zhangruifang <zhangruifang@h-partners.com> - 0.8.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: backport patches from upstream

* Mon Jan 29 2024 zhuofeng <zhuofeng2@huawei.com> - 0.8.0-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update version to 0.8.0

* Sun Dec 31 2023 Lv Ying <lvying6@huawei.com> - 0.6.8-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport bugfix patches from community

* Fri Dec 01 2023 renhongxun <renhongxun@h-partners.com> - 0.6.8-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:clear the link to rasdaemon.service when uninstalling rasdaemon
       and ensure rasdaemon restart once be upgraded

* Sat Jun 17 2023 yanglongkang <yanglongkang@h-partners.com> - 0.6.8-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport libtraceevent patch to adapt to kernel ftrace ring buffer change

* Fri Jun 2 2023 Shiju Jose<shiju.jose@huawei.com> - 0.6.8-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:
  1. Fix return value type issue of read/write function from unistd.h.
  2. Fix issue of signed and unsigned integer comparison.
  3. Remove redundant header file and do some cleaup.
  4. Add support for create/open the vendor error tables at rasdaemon startup.
  5. Make changes in the HiSilicon error handling code for the same.
  6. Add four modules supported by HiSilicon common section

* Fri Mar 31 2023 huangfangrun <huangfangrun1@h-partners.com> - 0.6.8-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:
  1.Fix for regression in ras_mc_create_table() if some cpus are offline at the system start.
  2.Fix poll() on per_cpu trace_pipe_raw blocks indefinitely.

* Fri Mar 24 2023 renhongxun <renhongxun@h-partners.com> - 0.6.8-2
- backport patches from upstream

* Thu Jan 19 2023 Lv Ying <lvying6@huawei.com> - 0.6.8-1
- backport bugfix patches from community:
  1. Fix error print handle_ras_events.

* Thu Aug 25 2022 Xiaofei Tan <tanxiaofei@huawei.com> - 0.6.7-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:
  Add the following patch to fix startup core dumped issue.
  0001-rasdaemon-use-standard-length-PATH_MAX-for-path-name.patch

* Mon May 23 2022 Shiju Jose<shiju.jose@huawei.com> - 0.6.7-5
- Type:feature
- ID:NA
- SUG:NA
- DESC:
  Update with the latest patches for the
  1. CPU online fault isolation for arm event.
  2. Modify recording Hisilicon common error data in the rasdaemon
  3. In the ras-mc-ctl,
  3.1. Improve Hisilicon common error statistics.
  3.2. Add support to display the HiSilicon vendor-errors for a specified module.
  3.3. Add printing usage if necessary parameters are not passed for the HiSilicon vendor-errors options.
  3.4. Reformat error info of the HiSilicon Kunpeng920.
  3.5. Relocate reading and display Kunpeng920 errors to under Kunpeng9xx.
  3.6. Updated the HiSilicon platform name as KunPeng9xx.
  4. Fixed a memory out-of-bounds issue in the rasdaemon.

* Mon Mar 07 2022 Shiju Jose<shiju.jose@huawei.com> - 0.6.7-4
- Type:feature
- ID:NA
- SUG:NA
- DESC:
  1. Modify recording Hisilicon common error data in the rasdaemon and
  2. In the ras-mc-ctl,
  2.1. Improve Hisilicon common error statistics.
  2.2. Add support to display the HiSilicon vendor-errors for a specified module.
  2.3. Add printing usage if necessary parameters are not passed for the HiSilicon vendor-errors options.
  2.4. Reformat error info of the HiSilicon Kunpeng920.
  2.5. Relocate reading and display Kunpeng920 errors to under Kunpeng9xx.

* Wed Mar 2 2022 tanxiaofei<tanxiaofei@huawei.com> - 0.6.7-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:
  1. Backport 4 patches from openEuler master branch.
  1) Fix the issue of sprintf data type mismatch in uuid_le()
  2) Fix the issue of command option -r for hip08
  3) Fix some print format issues for hisi common error section
  4) Add some modules supported by hisi common error section
  2.Enable compilation of the feature memory fault prediction based on
  corrected error.
  3.Fix changelog date error of this spec file.

* Wed Feb 23 2022 luoshengwei<luoshengwei@huawei.com> - 0.6.7-2
- Type:feature
- ID:NA
- SUG:NA
- DESC: Add cpu online fault isolation for arm event.

* Wed Dec 8 2021 xujing <xujing99@huawei.com> - 0.6.7-1
- Update software to v0.6.7

* Thu Jul 29 2021 tanxiaofei<tanxiaofei@huawei.com> - 0.6.6-6
- Type:feature
- ID:NA
- SUG:NA
- DESC:Add support for hisilicon common section that some IIO devices may
- used in new firmware of Kunpeng920, and Kunpeng930 will also use it too.

* Sat May 15 2021 xujing<17826839720@163.com> - 0.6.6-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix disk error log storm

* Wed Apr 28 2021 Lv Ying <lvying6@huawei.com> - 0.6.6-4
- backport bugfix patches from community:
  1. Fix error print handle_ras_events.

* Wed Mar 31 2021 Lv Ying <lvying6@huawei.com> - 0.6.6-3
- backport bugfix patches from community:
  1. ras-page-isolation: do_page_offline always considers page offline was successful
  2. ras-page-isolation: page which is PAGE_OFFLINE_FAILED can be offlined again

* Fri Sep 25 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.6.6-2
- Update software source URL

* Fri Jul 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.6.6-1
- Update software to v0.6.6

* Tue Feb 25 2020 lvying<lvying6@huawei.com> - 0.6.3-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix file descriptor leak in ras-report.c:setup_report_socket()

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.6.3-1
- Package init
